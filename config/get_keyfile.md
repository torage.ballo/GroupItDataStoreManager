# Download keyfile for this application
1. Navigate to google console IAM & ADMIN page
2. Click service and accounts (https://console.cloud.google.com/iam-admin/serviceaccounts?authuser=1&project=groupit-185516)
3. Click at the ... button under action of  groupit-185516@appspot.gserviceaccount.com
4. Click on the "Creat key" option
5. Choose json file format, and save it to this folder and update config.js with the correct filename

# Important, do not commit the keyfile to git!