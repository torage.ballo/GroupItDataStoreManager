var faker = require('faker');

module.exports = {
  addUserToDataStore: function (ds, user, userKey) {
    const entity = {
      key: userKey,
      data: user,
    };
    ds.upsert(entity).then(() => {
      console.log('Added user: ' + user.name)
    });
  },

  addUserFromArray: function (ds, users) {
    users.forEach(user => {
      const userKey = {
        "name": user.id,
        "kind": "USERS",
        "path": ["USERS", user.id]
      };
      module.exports.addUserToDataStore(ds, user, userKey)
    });
  },
  createRandomUser: function () {
    let firstname = faker.name.firstName();
    let lastname = faker.name.lastName();
    let randUser = {
      "id": faker.random.uuid(),
      "name": firstname + ' ' + lastname,
      "alias": firstname,
      "email": faker.internet.email(),
      "avatarUrl": faker.image.avatar()
    };
    return randUser;
  }
}