module.exports = {

  print: function (entities) {
    entities.forEach(entity => {
      console.log(entity);
    });
  },

  getEntities: async function (datastore, kind, order) {
    const query = datastore.createQuery("", kind).order(order);
    try {
      let entries = await datastore.runQuery(query);
      return entries[0];
    } catch (err) {
      console.error('ERROR:', err);
    }
  },

  deleteEntities: function (ds, entities) {
    entities.forEach(entity => {
      deleteEntityInDatastore(ds, entity[ds.KEY])
    })
  }
};


function deleteEntityInDatastore(ds, entityKey) {
  ds.delete(entityKey).then(() => {
    console.log('Deleted entity: ' + entityKey.name)
  });
}