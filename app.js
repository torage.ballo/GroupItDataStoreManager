const userUtiles = require("./js/userUtiles");
const Datastore = require('@google-cloud/datastore');
const entities = require("./js/entities");
const fs = require('fs');

const projectId = 'groupit-185516';
const keyFilename = 'config/groupit-185516-def5f46ebc2e.json'; //for å laste ned denne les get_keyfile.md

//const users = JSON.parse(fs.readFileSync('testdata/users.json', 'utf8'));
let users = [];
for  (i = 0; i< 30; i++){
  randuser = userUtiles.createRandomUser();
  users.push(randuser);
}


const datastore = new Datastore({
  projectId: projectId,
  keyFilename: keyFilename,
});


function sleep(time) {
  return new Promise((resolve => setTimeout(resolve, time)));
}


function slettAllEntities(kind, order){
  console.log('Starter å slette alle kinds av type: ' + kind + '...');
  let allUsers = entities.getEntities(datastore,kind, order);
  allUsers.then(users => {entities.deleteEntities(datastore, users)});
}

slettAllEntities('USERS', 'id');
slettAllEntities('USER_EVENTS', 'timeStampMs');
slettAllEntities('GROUP_EVENTS', 'timeStampMs');
slettAllEntities('USER_GROUP', 'groupId');

//veneter litt, siden gc ds ikke er en synkron tjeneste..
sleep(1000).then(() => {
    console.log('legger til brukere...');
    userUtiles.addUserFromArray(datastore, users);
});
